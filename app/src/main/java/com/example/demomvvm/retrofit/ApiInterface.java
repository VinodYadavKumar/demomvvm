package com.example.demomvvm.retrofit;



import com.example.demomvvm.HolidayModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiInterface {

    @GET("PublicHolidays/2019/us")
    Call<List<HolidayModel>> getHolidays();



}
